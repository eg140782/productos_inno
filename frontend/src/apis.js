import http from "./http-common";


const getAll = () => {
  return http.get("/todos/");
};

const getproductos = () => {
    return http.get("/productos/");
  };
  
const createproducto = (task) => {
    return http.post("/productos/", task);

}; 

const modificarproducto = (id, task) => {
    return http.put("/productos/${"+id+"}", task);

}; 

const getproductosdatos = (id) => {
    return http.get("/productos/${id}");
  };
export default {
  getAll,
  getproductos,
  createproducto,
  modificarproducto,
  getproductosdatos
};