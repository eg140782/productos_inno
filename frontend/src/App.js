import React, { Component } from "react"
import { BrowserRouter } from 'react-router-dom'
import { Route, Link ,Routes} from 'react-router-dom'
import './App.css';
import CustomersList  from "./componentes/categorias"
import ProductosList  from "./componentes/productos"
import ProductosCreate  from "./componentes/producto_create"
import Productosupdate  from "./componentes/producto_create"

const BaseLayout = () => (
  <div className="container-fluid">
<nav className="navbar navbar-expand-lg navbar-light bg-light">
  <a className="navbar-brand" href="/">Django React Demo</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div className="navbar-nav">
      <a className="nav-item nav-link" href="/categorias">Categorias</a>
      <a className="nav-item nav-link" href="/producto">Productos</a>

    </div>
  </div>
</nav>  

    <div className="content">

    <Routes>
        <Route path="/categorias" element={<CustomersList />} /> 
        <Route path="/producto" element={<ProductosList />} /> 
        <Route path="/productoCreate" element={<ProductosCreate />} /> 
        <Route path="/productoupdate" element={<Productosupdate />} /> 

    </Routes>
    </div>

  </div>
)


class App extends Component {



    render() {
      return (
        
        
            <BrowserRouter>
      
              <BaseLayout/>
           
            </BrowserRouter>


      )
    }
  }
  
export default App;