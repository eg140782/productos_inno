

import React, { useState, useEffect } from "react";
import CustomersService from "../apis";


const CustomersList = (props) => {

  const [categoria, setCategoria] = useState([]);
  
  const retrieveCategoria = () => {
    CustomersService.getAll()
      .then((response) => {
        setCategoria( response.data);
        console.log(response.data)
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    retrieveCategoria();
  }, []);

  return (
    
    <div className="container">
      <div className="row">
        <div className="col-4">
          <h5>Listado de Categorias</h5> 
        </div>
      </div>
        <table className="table table-striped table-bordered mt-4">
          <thead>
            <tr className="text-center">
              <th>ID</th>
              <th>nombre</th>
              <th>estado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {categoria.map(
              (ca)=>(
              <tr key={ca.id}>
                <td className="text-center">{ca.id}</td>
                <td>{ca.nombre}</td>
                <td className="text-center">{ca.estado}</td>
                <td className="text-center">

                </td>
              </tr>
            ))}
          </tbody>
        </table> 
      
    </div>                
  );
  
};
export default CustomersList;