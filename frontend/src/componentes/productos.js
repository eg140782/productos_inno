import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import CustomersService from "../apis";

const ProductosList = (props) => {
    const navigate = useNavigate();

  const [producto, setProducto] = useState([]);
  
  const retrieveProducto = () => {
    CustomersService.getproductos()
      .then((response) => {
        setProducto( response.data);
        console.log(response.data)
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    retrieveProducto();
  }, []);

  
  return (
    
    <div className="container">
      <div className="row">
        <div className="col-4">
          <h5>Listado de Productos</h5> 
          
            <Link to={"/productoCreate"} >
                Agregar Nuevo Producto
            </Link>
        </div>
      </div>
        <table className="table table-striped table-bordered mt-4">
          <thead>
            <tr className="text-center">
              <th>ID</th>
              <th>Nombre</th>
              <th>Estado</th>
              <th>Categoria</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {producto.map(
              (ca)=>(
              <tr key={ca.id}>
                <td className="text-center">{ca.id}</td>
                <td>{ca.nombre}</td>
                <td className="text-center">{ca.estado}</td>
                <td className="text-center">{ca.categoria}</td>
                <td className="text-center">
                    
                </td>

              </tr>
            ))}
          </tbody>
        </table> 
      
    </div>                
  );
  
};
export default ProductosList;