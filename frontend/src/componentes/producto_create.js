import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import CustomersService from "../apis";
import { toast } from "react-hot-toast";



const ProductosCreate = (props) => {
    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
      } = useForm();
      const navigate = useNavigate();
      const params = useParams();

    const [categoria, setcategoria] = useState([]);
    
    const retrievecategoria = () => {
      CustomersService.getAll()
        .then((response) => {
          setcategoria( response.data);
          console.log(response.data)
        })
        .catch((e) => {
          console.log(e);
        });
    };
  
    useEffect(() => {
        retrievecategoria();
    }, []);
  
    const onSubmit = handleSubmit(async (data) => {
        // if (params.id) {
        //   await CustomersService.modificarproducto(params.id, data);
        //   toast.success("Task updated", {
        //     position: "bottom-right",
        //     style: {
        //       background: "#101010",
        //       color: "#fff",
        //     },
        //   });
        // } else {
        await CustomersService.createproducto(data);
        toast.success("New Task Added", {
        position: "bottom-right",
        style: {
            background: "#101010",
            color: "#fff",
        },
        });
        // } 
    
        navigate("/producto");
      });

    // useEffect(() => {
    // async function loadTask() {
    //     if (params.id) {
    //         const { data } = await CustomersService.getproductosdatos(params.id);
    //         setValue("nombre", data.nombre);
    //         setValue("categoria", data.categoria);
    //         setValue("estado", data.estado);
    //     }
    // }
    // loadTask();
    // }, []);

    const initialPlanState = {
        id: null,
        nombre: ""
      };
    const [planCurrent, setPlanCurrent] = useState(initialPlanState);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setPlanCurrent({ ...planCurrent, [name]: value });
    };
    
    return (
      <div className="container">
        sdsd
        <div className="row">
          <div className="col-4">
            <h5>Agregar de Productos</h5> 
            <form onSubmit={onSubmit} className="bg-zinc-800 p-10 rounded-lg mt-2" >
                <label>Nombre del Producto:</label>
                <input style={{fontSize: '12px'}}
                type="text"
                {...register("nombre", { required: true })}
                className="bg-zinc-700 p-3 rounded-lg block w-full mb-3"
                autoFocus
                />

                {errors.nombre && <span>This field is required</span>}

                <div>
                <label>
                    Categoria:</label>
                 
                    <select onChange={handleInputChange} name="categoria" id="categoria" className="form-select form-select" aria-label=".form-select" style={{fontSize: '12px'}}
                    {...register("categoria", { required: true })}>
                    <option defaultValue="Selec">Seleccione...</option>
                        { categoria.map(p => (
                                <option key={p.id} value={p.id}>{p.nombre}</option>
                        ))} 
                    </select>
        
                    
                </div>

                {errors.categoria && <span>This field is required</span>}

                <div>
                <label>
                    Estado:</label>
                 
                    <select name="estado" id="estado" className="form-select form-select" aria-label=".form-select" style={{fontSize: '12px'}}
                    {...register("estado", { required: true })}>
                        <option value="A">Activo</option>
                        <option value="I">Inactivo</option>
                    </select>
        
                    
                </div>
                {errors.estado && <span>This field is required</span>}
                <br></br>

                <button className="bg-indigo-500 p-3 rounded-lg block w-full mt-3">
                Save
                </button>
            </form>

            {params.id && (
        <div className="flex justify-end">
{/*           <button
            className="bg-red-500 p-3 rounded-lg w-48 mt-3"
            onClick={async () => {
              const accepted = window.confirm("Are you sure?");
              if (accepted) {
                await deleteTask(params.id);
                toast.success("Task Removed", {
                  position: "bottom-right",
                  style: {
                    background: "#101010",
                    color: "#fff",
                  },
                });
                navigate("/tasks");
              }
            }}
          >
            delete
          </button> */}
        </div>
      )}
          </div>
        </div>
          
        
      </div>                
    );
    
  };
  export default ProductosCreate;