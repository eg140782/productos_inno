from django.db import models

# Create your models here.


class Categorias(models.Model):
    nombre = models.CharField(max_length=100)
    estado = models.CharField(max_length=1, default='A')
    def __str__(self):
        return str(self.nombre) 


class Productos(models.Model):
   nombre = models.CharField(max_length=100)
   estado = models.CharField(max_length=1, default='A')
   categoria = models.ForeignKey(Categorias, null=True, blank=True, on_delete=models.PROTECT)

class Productos_imagenes(models.Model):
   producto = models.ForeignKey(Productos, null=True, blank=True, on_delete=models.PROTECT)
   estado = models.FileField(upload_to='imagenes/', null=True, blank=True)


