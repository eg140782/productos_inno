from django.contrib import admin

# Register your models here.
from negocios.models import *

class CategoriaAdmin(admin.ModelAdmin):
  list = ('nombre', 'estado')

  admin.site.register(Categorias)