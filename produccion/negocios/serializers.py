from rest_framework import serializers
from negocios.models import *




class CategoriasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categorias
        fields = ('id' ,'nombre', 'estado')


class ProductosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Productos
        fields = ('id' ,'nombre', 'estado', 'categoria')