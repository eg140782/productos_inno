from django.urls import path, include
from django.contrib.auth import views as auth_views
from rest_framework import routers
from negocios import views   

router = routers.DefaultRouter()                   
router.register(r'todos', views.CategoriasView, 'todo')  
router.register(r'productos', views.ProductosView, 'productos') 
urlpatterns =[
    #  path('categorias/list/', CategoriasView, name='CategoriasView'),
    path('api/', include(router.urls)) 

]