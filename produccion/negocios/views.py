from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from negocios.serializers import CategoriasSerializer, ProductosSerializer
from rest_framework import viewsets      
from negocios.models import *
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic


class CategoriasView(viewsets.ModelViewSet):  
    serializer_class = CategoriasSerializer   
    queryset = Categorias.objects.all()  

class ProductosView(viewsets.ModelViewSet):  
    serializer_class = ProductosSerializer   
    queryset = Productos.objects.all()  

